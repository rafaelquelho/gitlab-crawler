build:
	docker-compose build

run-crawler:
	docker-compose run -e OPTION=crawler app

run-report:
	docker-compose run -e OPTION=report app

test:
	docker-compose run app pytest

.PHONY: build run-crawler run-report test