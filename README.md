## GitLab Crawler

A **crawler** application for GitLab that provides:

- Crawl commit history for a repository
- Store the commit history in a secure containerized database
- Automated report generation

Given a **group name** and **branch**, **GitLab Crawler** is able to crawl the entire commit history in batches.

Before crawling, **GitLab Crawler** checks with ease if the GitLab branch is even with the commit history stored in the database so it does not need to read the commit history unnecessarily.

## Contents

- [Contents](#contents)
- [Requirements](#requirements)
- [Configuration](#configuration)
- [How to install](#how-to-install)
- [How to crawl](#how-to-crawl)
- [How to generate report](#how-to-generate-report)
- [Run tests](#run-tests)
- [GitLab login](#gitLab-login)

## Requirements

- Docker
- Docker compose

## Configuration

Copy the `.env-sample` file and change the enviroment variables:

```bash
$ cp .env-sample .env
```

## How to install

```bash
$ make build
```

## How to crawl

```bash
$ make run-crawler
```

## How to generate report

```bash
$ make run-report
```

## Run tests
```sh
$ make test
```

## GitLab login

At this point of the project, GitLab has a security layer (Cloudflare) that avoids login attempts from crawling codes. Basically, in order to login, one should solve the Cloudflare challenge and provide the `jschl_answer` in order to access the GitLab login page, which is not an easy task to bypass Cloudflare firewall.

If this layer was not present, a possible solution for login is presented at `/app/authentication.py`.