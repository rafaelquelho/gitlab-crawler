import os
import logging
from handlers.handle_report import HandleReport
from handlers.handle_crawl import HandleCrawler

logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    option = os.environ.get("OPTION")

    if option == "crawler":
        handle_crawl = HandleCrawler()
        handle_crawl.run_crawler()
    elif option == "report":
        handle_report = HandleReport()
        handle_report.create_report()
