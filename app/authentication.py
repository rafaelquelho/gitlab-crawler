import requests
from urllib.parse import urljoin
import bs4
from time import sleep

base_uri = "https://gitlab.com/"


class Authentication:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def login(self):

        session = requests.session()

        response = session.get(
            urljoin(base_uri, "/users/sign_in"), allow_redirects=True
        )
        response.raise_for_status()

        soup = bs4.BeautifulSoup(response.text, "html.parser")

        csrf_param = soup.find("meta", dict(name="csrf-param"))["content"]
        csrf_token = soup.find("meta", dict(name="csrf-token"))["content"]
        request_data = {
            "user[login]:": self.username,
            "user[password]:": self.password,
            csrf_param: csrf_token,
            "user[remember_me]": 0,
        }

        response = session.post(response.url, data=request_data)
        response.raise_for_status()
