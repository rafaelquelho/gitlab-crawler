class Commit:
    def __init__(self, hash, username, date, message, created_at):
        self.hash = hash
        self.username = username
        self.date = date
        self.message = message
        self.created_at = created_at

    def __repr__(self):
        return f"{self.hash} {self.username} {self.date} {self.message} {self.created_at}\n"
