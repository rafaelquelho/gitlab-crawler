import logging
from urllib.parse import urljoin
import requests
import bs4
from database.repository import Repository
from datetime import datetime
from crawler.commit import Commit

BASE_URL = "https://gitlab.com"


class GitlabCrawler:
    def __init__(self, group_name, repository_name, branch_name):
        """Creates a gitlab crawler instance."""
        self.validate_crawler(group_name, repository_name, branch_name)

        self.group_name = group_name
        self.repository_name = repository_name
        self.branch_name = branch_name
        self.commit_history_base_url = self.get_commit_history_base_url()

        self.session = requests.session()

    def get_commit_history_base_url(self):
        """Builds the base gitlab commit history url

        Args:
            None

        Returns:
            commit_history_base_url [str]
        """
        return "/".join(
            (
                BASE_URL,
                self.group_name,
                self.repository_name,
                "-/commits",
                self.branch_name,
            )
        )

    def get_commit_history_url(self, limit, offset):
        """Builds the gitlab commit history url with pagination

        Args:
            limit [int]
            offset [int]

        Returns:
            commit_history_url [str]
        """
        return urljoin(self.commit_history_base_url, f"?limit={limit}&offset={offset}")

    def get_commit_history(self, limit, offset, hash_to_stop):
        """Gets the entire gitlab commit history until it ends or
        reaches the hash_to_stop.

        Args:
            limit [int]
            offset [int]
            hash_to_stop [str]

        Returns:
            commits [List[Commit]]
        """
        self.validate_page_limit(limit)

        commits = []
        while True:
            response_commits = self.get_commits(limit, offset)

            for commit in response_commits:
                if commit.hash == hash_to_stop:
                    return commits

                commits.append(commit)

            if len(response_commits) < limit:
                break

            offset += limit

        return commits

    def get_commits(self, limit, offset):
        """Fetches commit history from gitlab using http request.

        Args:
            limit [int]
            offset [int]

        Returns:
            commits [List[Commit]]
        """
        logging.info(f"Crawling batch of {limit} commits")

        response = self.session.get(self.get_commit_history_url(limit, offset))
        response.raise_for_status()

        logging.info(f"Batch crawled successfully")

        return self.parse_response(response)

    def parse_response(self, response):
        """Parses the html response into the Commit object.

        Args:
            response [response]

        Returns:
            commits [List[Commit]]
        """
        soup = bs4.BeautifulSoup(response.text, "html.parser")

        commits = []

        commit_contents = soup.find_all("div", class_="commit-content")
        for commit_content in commit_contents:
            commit_row_message = commit_content.find(class_="commit-row-message")

            commit_message = commit_row_message.getText()
            commit_hash = commit_row_message["href"].split("/")[-1]
            commit_date = commit_content.find("time", class_="js-timeago")["datetime"]

            username = commit_content.find("a", class_="commit-author-link").getText()

            commit_date = datetime.strptime(commit_date, "%Y-%m-%dT%H:%M:%SZ")

            commit = Commit(
                commit_hash, username, commit_date, commit_message, datetime.now()
            )
            commits.append(commit)

        return commits

    def should_crawl(self, last_commit_db_hash):
        """Compares the last commit from database with last commit from gitlab.

        Args:
            last_commit_db_hash [str]

        Returns:
            should_crawl [bool]
        """
        if not last_commit_db_hash:
            return True, None

        hash_head_gitlab = self.get_commits(1, 0)[0].hash

        return last_commit_db_hash != hash_head_gitlab, last_commit_db_hash

    def validate_crawler(self, group_name, repository_name, branch_name):
        """Validates the arguments used to create the GitlabCrawler object.

        Args:
            group_name [str]
            repository_name [str]
            branch_name [str]

        Returns:
            None
        """
        if not group_name or not repository_name or not branch_name:
            raise Exception("Invalid arguments for GitLab crawler")

    def validate_page_limit(self, limit):
        """Validates the pagination limit.

        Args:
            limit [int]

        Returns:
            None
        """
        if not limit or limit < 0:
            raise Exception("Invalid limit value")
