import os
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, MetaData
from sqlalchemy.sql.expression import select
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Boolean, DateTime, Integer


class Repository:
    def __init__(self):
        """Creates a connection to database and creates tables if necessary."""
        db_pass = os.environ.get("POSTGRES_PASSWORD")
        db_user = os.environ.get("POSTGRES_USER")
        db_name = os.environ.get("POSTGRES_DB")
        db_port = os.environ.get("POSTGRES_PORT")

        db_host = "db"

        connection_string = (
            f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        )
        self.db = create_engine(connection_string)
        self.get_tables()

        self.create_fixtures()

    def get_tables(self):
        """Defines the commits and crawl_history tables."""
        meta = MetaData(self.db)
        self.commits_table = Table(
            "commits",
            meta,
            Column("id", Integer, primary_key=True, autoincrement=True),
            Column("hash", String),
            Column("username", String),
            Column("date", DateTime),
            Column("message", String),
            Column("created_at", DateTime),
        )

        self.crawl_history_table = Table(
            "crawl_history",
            meta,
            Column("id", Integer, primary_key=True, autoincrement=True),
            Column("last_commit_id", Integer, ForeignKey("commits.id"), nullable=False),
            Column("last", Boolean),
        )

    def create_fixtures(self):
        """Creates the tables in database."""
        with self.db.connect() as conn:
            self.commits_table.create(checkfirst=True)
            self.crawl_history_table.create(checkfirst=True)

    def insert_commit(self, commit):
        """Insert commit in database."""
        with self.db.connect() as conn:
            insert_statement = self.commits_table.insert().values(
                hash=commit.hash,
                username=commit.username,
                date=commit.date,
                message=commit.message,
                created_at=commit.created_at,
            )

            conn.execute(insert_statement)

    def update_crawl_history(self, last_commit_hash):
        """Insert data into crawl_history table."""
        with self.db.connect() as conn:
            update_statement = (
                self.crawl_history_table.update()
                .where(self.crawl_history_table.c.last == True)
                .values(last=False)
            )

            conn.execute(update_statement)

            find_last_commit_id_statment = self.commits_table.select().where(
                self.commits_table.c.hash == last_commit_hash
            )

            last_commit = conn.execute(find_last_commit_id_statment).fetchone()
            if not last_commit:
                return

            last_commit_id = last_commit._mapping["id"]

            insert_statement = self.crawl_history_table.insert().values(
                last_commit_id=last_commit_id, last=True
            )

            conn.execute(insert_statement)

    def find_last_commit_hash(self):
        """Finds the hash from the last commit present in the commit history.

        Args:
            None

        Returns:
            commit_hash [str]
        """
        with self.db.connect() as conn:
            select_statement = self.commits_table.select()
            select_statement = select_statement.where(
                self.crawl_history_table.c.last == True
            )

            join = self.commits_table
            table_list = [self.crawl_history_table]
            for table in table_list:
                join = join.join(table)
            select_statement = select_statement.select_from(join)

            last_commit = conn.execute(select_statement).fetchone()
            if not last_commit:
                return None

            last_commit_hash = last_commit._mapping["hash"]
            return last_commit_hash

    def find_user_more_commits(self):
        """Finds username and commit count from user with more commits.

        Args:
            None

        Returns:
            username [str]
            count_commits [int]
        """
        with self.db.connect() as conn:
            count_statement = func.count(self.commits_table.c.username).label(
                "count_commits"
            )

            select_statement = (
                select(
                    [
                        self.commits_table.c.username,
                        count_statement,
                    ]
                )
                .select_from(self.commits_table)
                .group_by(self.commits_table.c.username)
                .order_by(count_statement.desc())
                .limit(1)
            )

            userdata = conn.execute(select_statement).fetchone()
            if not userdata:
                return None, None

            return userdata._mapping["username"], userdata._mapping["count_commits"]

    def find_last_commit(self, username):
        """Finds hash and date from last commit made by the user.

        Args:
            username [str]

        Returns:
            hash [str]
            date [datetime]
        """
        with self.db.connect() as conn:
            select_statement = (
                select([self.commits_table.c.hash, self.commits_table.c.date])
                .where(self.commits_table.c.username == username)
                .order_by(self.commits_table.c.date.desc())
                .limit(1)
            )

            userdata = conn.execute(select_statement).fetchone()
            if not userdata:
                return None, None

            return userdata._mapping["hash"], userdata._mapping["date"]
