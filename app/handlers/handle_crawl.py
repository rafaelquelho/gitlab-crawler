import os
import logging
from database.repository import Repository
from crawler.gitlab_crawler import GitlabCrawler


class HandleCrawler:
    def __init__(self):
        self.repository = Repository()
        self.group_name = os.environ.get("GROUP_NAME")
        self.repository_name = os.environ.get("REPOSITORY_NAME")
        self.branch_name = os.environ.get("BRANCH_NAME")
        self.limit = int(os.environ.get("LIMIT", 10))

    def run_crawler(self):
        """Runs the crawler fetching a GitLab repository commits.
        First, it checks if needs to crawl, comparing the database history
        to GitLab history.
        Then, the commits are inserted in the database.

        Args:
            None

        Returns:
            None
        """
        try:
            logging.info("Crawler initiated...")
            logging.info("---------------------------------------------------------")

            crawler = GitlabCrawler(
                self.group_name, self.repository_name, self.branch_name
            )

            last_commit_db_hash = self.repository.find_last_commit_hash()

            should_crawl, hash_head_db = crawler.should_crawl(last_commit_db_hash)

            if not should_crawl:
                logging.info(
                    "No new commits were found in GitLab. Crawler will not run"
                )
                return

            new_commits = crawler.get_commit_history(self.limit, 0, hash_head_db)
            for commit in new_commits:
                self.repository.insert_commit(commit)

            self.repository.update_crawl_history(new_commits[0].hash)

            logging.info("Crawler finished...")
            logging.info("---------------------------------------------------------")
        except Exception as error:
            logging.error(error)
