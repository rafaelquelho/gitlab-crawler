import logging
from database.repository import Repository


class HandleReport:
    def __init__(self):
        self.repository = Repository()

    def create_report(self):
        """Creates the report based on the commit history present in the database.
        It provides information about the user that has more commits.

        Args:
            None

        Returns:
            None
        """
        try:
            logging.info("Crawler report initiated...")

            self.get_report_most_frequent_committer()
        except Exception as error:
            logging.error(error)

    def get_report_most_frequent_committer(self):
        """Provides information about the user that has more commits:
        username, number of commits, last commit date, last commit hash.

        Args:
            None

        Returns:
            None
        """
        username, commit_number = self.repository.find_user_more_commits()
        if not username or not commit_number:
            logging.info("User with more commits was not found")
            return None

        logging.info("---------------------------------------------------------")
        logging.info("Report for user with more commits:")
        logging.info(f'Username: "{username}"')
        logging.info(f"Number of commits: {commit_number}")

        hash, date = self.repository.find_last_commit(username)
        if not hash or not date:
            logging.error(
                f'It was not possible to find the last commit from user "{username}"'
            )
            return

        logging.info(f"Last commit date: {date}")
        logging.info(f"Last commit hash: {hash}")
        logging.info("---------------------------------------------------------")
