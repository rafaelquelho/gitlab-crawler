import datetime
import pytest
from crawler.gitlab_crawler import GitlabCrawler
from crawler.commit import Commit


def test_get_commit_history_base_url():
    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    expected_base_url = "https://gitlab.com/rafaelquelho/gitlab-crawler/-/commits/main"
    assert crawler.get_commit_history_base_url() == expected_base_url


def test_get_commit_history_url():
    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    limit = 20
    offset = 0

    got_commit_history_url = crawler.get_commit_history_url(limit, offset)

    expected_base_url = "https://gitlab.com/rafaelquelho/gitlab-crawler/-/commits/main"
    expected_commit_url = f"?limit={limit}&offset={offset}"

    assert got_commit_history_url == expected_base_url + expected_commit_url


def test_get_commit_history_should_raise_error():
    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    with pytest.raises(Exception):
        crawler.get_commit_history(-20, 0)


@pytest.mark.parametrize(
    "group_name,repository_name,branch_name",
    [
        ("", "repo", "branch"),
        (None, "repo", "branch"),
        ("group", "", "branch"),
        ("group", None, "branch"),
        ("group", "repo", ""),
        ("group", "repo", None),
    ],
)
def test_create_invalid_crawler_should_raise_error(
    group_name, repository_name, branch_name
):
    with pytest.raises(Exception):
        GitlabCrawler(group_name, repository_name, branch_name)


def test_get_commit_history_when_commit_history_is_empty(mocker):
    mock_resp = mocker.Mock()
    mock_resp.raise_for_status = mocker.Mock()
    mock_resp.content = "foo"

    mocker_session = mocker.patch("crawler.gitlab_crawler.requests.session")
    mocker_session.return_value = mocker.MagicMock(
        get=mocker.MagicMock(return_value=mock_resp)
    )

    mocker_parse = mocker.patch.object(
        GitlabCrawler, "parse_response", side_effect=[[]]
    )

    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    got_commit_history = crawler.get_commit_history(20, 0, None)

    assert len(got_commit_history) == 0

    mocker_parse.assert_called_with(mock_resp)


def test_get_commit_history(mocker):
    mock_resp = mocker.Mock()
    mock_resp.raise_for_status = mocker.Mock()
    mock_resp.content = "foo"

    mocker_session = mocker.patch("crawler.gitlab_crawler.requests.session")
    mocker_session.return_value = mocker.MagicMock(
        get=mocker.MagicMock(return_value=mock_resp)
    )

    commit_date = datetime.datetime(2020, 12, 1)
    created_at = datetime.datetime(2021, 12, 1)

    commit_history_calls = [
        [
            Commit("hash1", "user1", commit_date, "message1", created_at),
            Commit("hash2", "user1", commit_date, "message2", created_at),
        ],
        [
            Commit("hash3", "user3", commit_date, "message3", created_at),
            Commit("hash4", "user2", commit_date, "message4", created_at),
        ],
        [Commit("hash5", "user5", commit_date, "message5", created_at)],
    ]

    mocker_parse = mocker.patch.object(
        GitlabCrawler, "parse_response", side_effect=commit_history_calls
    )

    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    got_commit_history = crawler.get_commit_history(2, 0, None)

    assert len(got_commit_history) == 5

    mocker_parse.assert_called_with(mock_resp)

    assert mocker_parse.call_count == 3


def test_get_commit_history_with_hashstop(mocker):
    mock_resp = mocker.Mock()
    mock_resp.raise_for_status = mocker.Mock()
    mock_resp.content = "foo"

    mocker_session = mocker.patch("crawler.gitlab_crawler.requests.session")
    mocker_session.return_value = mocker.MagicMock(
        get=mocker.MagicMock(return_value=mock_resp)
    )

    commit_date = datetime.datetime(2020, 12, 1)
    created_at = datetime.datetime(2021, 12, 1)

    commit_history_calls = [
        [
            Commit("hash1", "user1", commit_date, "message1", created_at),
            Commit("hash2", "user1", commit_date, "message2", created_at),
        ],
        [
            Commit("hash3", "user3", commit_date, "message3", created_at),
            Commit("hash4", "user2", commit_date, "message4", created_at),
        ],
        [Commit("hash5", "user5", commit_date, "message5", created_at)],
    ]

    mocker_parse = mocker.patch.object(
        GitlabCrawler, "parse_response", side_effect=commit_history_calls
    )

    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    got_commit_history = crawler.get_commit_history(2, 0, "hash3")

    assert len(got_commit_history) == 2

    mocker_parse.assert_called_with(mock_resp)

    assert mocker_parse.call_count == 2


def test_parse_response(mocker):
    mock_resp = mocker.Mock()
    mock_resp.text = """
    <div class="commit-content" data-qa-selector="commit_content">
    <a class="commit-row-message item-title js-onboarding-commit-item " 
    href="/rafaelquelho/gitlab-crawler/-/commit/caa22f3f9faa7f3e7e7912b3dc5089d5e9f31f82">Initial commit</a>
    <span class="commit-row-message d-inline d-sm-none">
    ·
    caa22f3f
    </span>
    <div class="committer">
    <a class="commit-author-link js-user-link" data-user-id="9293728" href="/rafael.quelho" 
    title="">Rafael Quelho de Macedo</a> authored <time class="js-timeago" title="Jul 10, 2021 3:25pm GMT-0300" 
    datetime="2021-07-10T18:25:59Z" data-toggle="tooltip" data-placement="bottom" data-container="body">17 hours ago</time>
    </div>
    </div>
    """

    expected_message = "Initial commit"
    expected_hash = "caa22f3f9faa7f3e7e7912b3dc5089d5e9f31f82"
    expected_date = datetime.datetime(2021, 7, 10, 18, 25, 59)
    expected_username = "Rafael Quelho de Macedo"

    crawler = GitlabCrawler("rafaelquelho", "gitlab-crawler", "main")

    got_commits = crawler.parse_response(mock_resp)
    first_commit = got_commits[0]

    assert len(got_commits) == 1
    assert first_commit.message == expected_message
    assert first_commit.hash == expected_hash
    assert first_commit.date == expected_date
    assert first_commit.username == expected_username
