#!/bin/bash
# wait-for-postgres.sh

i=1; 

echo "Establishing connection to database, please wait..."; 
echo "Attempt $i"; 

while !</dev/tcp/db/${POSTGRES_PORT}; 
do sleep 1; 
let i+=1;
echo "Attempt $i"; 
done;

echo "Database connection established"; 

exec "$@"